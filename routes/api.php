<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('users')
    ->group(function () {
        Route::post('register', 'AuthController@register');
        Route::post('login', 'AuthController@login');
        Route::prefix('/{user}')
            ->middleware('auth:api')
            ->group(function() {
                Route::post('updatecredentials', 'UserController@updateCredentials');
                Route::prefix('cv')->group(function() {
                    Route::post('profile-pic', 'CvController@updateProfilePic');
                    Route::post('/', 'CvController@store');
                });
                // must be last route to retrieve user data
                Route::get('/', 'UserController@get');
            });
        
        
    });
