<?php

namespace App\Http\Middleware;

use Closure;

class QueryToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->query('authToken')) {
            $request->merge(['token'=> $request->query('authToken')]);
        }
        return $next($request);
    }
}
