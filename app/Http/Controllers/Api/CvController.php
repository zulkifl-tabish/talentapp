<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Resources\UserResource;
use App\Http\Requests\Api\ProfilePicRequest;
use App\Http\Requests\Api\CvRequest;
use Illuminate\Support\Facades\DB;

class CvController extends ApiController
{
    /**
     * Uodates user profile Pic
     *
     * @param  ProfilePicRequest $request
     * @return UserResource
     */
    public function updateProfilePic(ProfilePicRequest $request)
    {
        $user = $request->user();
       
        $profile = $user->profile;
        
        $profile->pic = $request->file('profilePic');

        $profile->save();
        
        $user->loadProfile();

        return new UserResource($user);
    }

    /**
     * Stores CV of logged User
     *
     * @param CvRequest $request
     * @return void
     */
    public function store(CvRequest $request) {

        return DB::transaction(function() use ($request) {

            $this->_updateCv($request);
            $this->_updateExperience($request);
            $this->_updateAddress($request);

            $user = $request->user();
            
            if(is_array($request->input('universities'))) {
                $user->syncRelationByColumn(
                    'universities', 'name', $request->input('universities')
                );
            }
            if(is_array($request->input('stipendium'))) {
                $user->syncRelationByColumn(
                    'stipendiums', 'name', $request->input('stipendium')
                );
            }

            $user->loadProfile();
            
            return new UserResource($user);
        });
    }

    /**
     * Creates or udpates user Address
     *
     * @param CvRequest $request
     * @return void
     */
    private function _updateAddress(CvRequest $request) {

        $user = $request->user();

        $address_fields = $request->input('address');
        if(is_array($address_fields)) {
            $user->address()->updateOrCreate([
                'user_id' => $user->id,
            ], $address_fields);
        }
    }
    
    /**
     * Creates or udpates user CV
     *
     * @param CvRequest $request
     * @return void
     */
    private function _updateCv(CvRequest $request) {
        $user = $request->user();
        
        $cv_fields = $request->only([
            'status',
            'phd',
            'abitur',
            'birthday',
            'salaryDesired'
        ]);

        $user->cv()->updateOrCreate([
            'user_id' => $user->id,
        ], $cv_fields);
    }


    /**
     * Updates experience of logged user
     * 
     *
     * @param CvRequest $request
     * @return void
     */
    private function _updateExperience(CvRequest $request) {

        $submitted_experiences  = $request->input('experience');

        if(is_null($submitted_experiences)) {
            return;
        }

        $user = $request->user();

        $existing_experience_ids = $user->experiences()
                                    ->pluck('id')
                                    ->toArray();

        $submitted_experience_ids = [];
        $new_experiences = [];

        if($submitted_experiences) {

            $submitted_experience_ids = array_column($submitted_experiences, 'id');

            foreach($submitted_experiences as $experience) {
                if(isset($experience['id'])) {
                    $user->experiences()
                        ->where('id', $experience['id'])
                        ->update($experience);
                } else {
                    array_push($new_experiences, $experience); 
                }
            }

            if($new_experiences) {
                $user->experiences()
                    ->createMany($new_experiences);
            };
        }

       
        if(
            is_array($submitted_experience_ids) 
            && count($submitted_experience_ids) > 0
        ) {
            $deleted_experiences = array_diff(
                $existing_experience_ids, $submitted_experience_ids
            );
        } else {
            $deleted_experiences = $existing_experience_ids;
        }

        if($deleted_experiences) {
            $user->experiences()->whereIn('id', $deleted_experiences)->delete();
        }
    }

}
