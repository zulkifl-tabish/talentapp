<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\RegisterRequest;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }


    /**
     * Registers new user
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $attributes = $request->only(['mail', 'firstName', 'lastName']);
        $attributes['password'] = Hash::make($request->input('password'));

        $user = User::create($attributes);

        if (!$user) {
            abort(500);
        }

        $user->profile()->create($request->only(['firstName', 'lastName']));

        return $this->respondWithToken($this->getGuard()->login($user), $user);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->only(['mail', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return new JsonResponse(['code' => 0, 'message' => 'Unauthorized'], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     * @param User|null $user
     * @return JsonResponse
     */
    protected function respondWithToken($token, User $user = null)
    {
        /** @var User $user */
        $user = $user ?? $this->getGuard()->user();

        return new JsonResponse([
            'authToken' => $token
        ]);
    }

    /**
     * @return \Tymon\JWTAuth\JWTGuard|\Tymon\JWTAuth\JWT
     */
    protected function getGuard()
    {
        return Auth::guard('api');
    }
}
