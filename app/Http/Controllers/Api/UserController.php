<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserResource;
use App\Http\Requests\Api\UpdateCredentialsRequest;


class UserController extends ApiController
{
    /**
     * Retrieves user
     *
     * @param  CvRequest $request
     * @return CvResource
     */
    public function get()
    {
        $user = request()->user();
        $user->loadProfile();

        return new UserResource($user);
    }

    /**
     * Updates user password and email
     *
     * @param UpdateCredentialsRequest $request
     * @return void
     */
    public function updateCredentials(UpdateCredentialsRequest $request)
    {
        $user = request()->user();
        $user->password = Hash::make($request->input('password'));
        $mail = $request->input('mail');
        if($mail && $mail !== $user->mail) {
            $user->mail = $request->input('mail');
        }

        $user->save();
        $user->loadProfile();

        return new UserResource($user);
    }
}
