<?php
namespace App\Http\Requests\Api;

class CvRequest extends ApiRequest
{

    /**
     * @return array
     */
    public function rules()
    {
 
        return [
            'universities' => 'sometimes|array',
            'universities.*' => 'string|required',
            'experience' => 'sometimes|array',
            'experience.*.id' => 'sometimes|exists:user_experiences,id,user_id,'.$this->user()->id,
            'experience.*.from' => 'required|date|before:' . date('Y-m-d') . '|date_format:Y-m-d',
            'experience.*.to' => 'sometimes|nullable|date|after:experience.*.from|date_format:Y-m-d',
            'experience.*.work' => 'required|string',
            'experience.*.employer' => 'required|string',
            'phd' => 'boolean',
            'status' => 'required|string',
            'stipendium' => 'sometimes|array',
            'stipendium.*' => 'required|string',
            'abitur' => 'required|numeric',
            'birthday'=> 'required|date_format:Y-m-d',
            'address' => 'required|array',
            'address.thoroughfare' => 'required|string',
            'address.premise' => 'required|string',
            'address.postalCode' => 'required|string',
            'address.locality' => 'required|string',
            'salaryDesired' => 'string|required'
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'universities' => ucfirst(__('universities')),
            'universities.*' => ucfirst(__('university name')),
            'phd' => __('PHD'),
            'experience' => ucfirst(__('experiences')),
            'experience.*.id' => ucfirst(__('experience ID')),
            'address.thoroughfare' => ucfirst(__('address thoroughfare')),
            'address.premise' => ucfirst(__('address premise')),
            'address.postalCode' => ucfirst(__('address postal code')),
            'address.locality' => ucfirst(__('address locality')),
            'salaryDesired' => ucfirst(__('desired salary')),
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'experience.*.id' => __('Experience is invalid.'),
        ];
    }
}
