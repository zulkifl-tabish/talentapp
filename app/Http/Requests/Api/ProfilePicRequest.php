<?php
namespace App\Http\Requests\Api;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class ProfilePicRequest extends ApiRequest
{

    /**
     * @return array
     */
    public function rules()
    {
       
        
        return [
            'profilePic' => 'required|image|max:2048',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'profilePic' => ucfirst(__('profile photo')),
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'profilePic.image' => __('Please provide an image file'),
            'profilePic.max' => __('File is too large, can only be 2Mb')
        ];
    }
}
