<?php
namespace App\Http\Requests\Api;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class UpdateCredentialsRequest extends ApiRequest
{

    /**
     * @return array
     */
    public function rules()
    {
        
        $uniqueMail = Rule::unique('users', 'mail')->ignore($this->user()->id, 'id');        
        
        $passwordVerify = function ($attribute, $value, $fail) {
            if (!Hash::check($value, $this->user()->password)) {
                $fail(__('Old password is invalid'));
            }
        };

        return [
            'mail' => [
                'required',
                'string',
                'email',
                'max:255',
                $uniqueMail
            ],
            'oldPassword' => ['required', $passwordVerify],
            'password' => 'required|string|min:6',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'mail' => ucfirst(__('e-mail address')),
            'password' => ucfirst(__('new password')),
            'oldPassword' => ucfirst(__('old password')),
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'mail.unique' => __('A user with the same e-mail address has already been registered.'),
            'oldPassword.invalid' => __('Please enter correct password'),
        ];
    }
}
