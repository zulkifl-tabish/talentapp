<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request as BaseRequest;
use Illuminate\Support\Facades\Auth;

abstract class ApiRequest extends BaseRequest
{
    public function authorize()
    {
        return Auth::guard('api')->check();
    }
}
