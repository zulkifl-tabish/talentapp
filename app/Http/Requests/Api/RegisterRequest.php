<?php

namespace App\Http\Requests\Api;

class RegisterRequest extends ApiRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return !parent::authorize();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'mail' => 'required|string|email|max:255|unique:users,mail',
            'password' => 'required|string|min:6',
            'firstName' => 'required|string|min:3',
            'lastName' => 'required|string|min:3',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'firstName' => ('First Name'),
            'lastName' => ('Last Name'),
            'mail' => ucfirst(__('e-mail address')),
            'password' => ucfirst(__('password')),
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'mail.unique' => __('A user with the same e-mail address has already been registered.'),
        ];
    }
}
