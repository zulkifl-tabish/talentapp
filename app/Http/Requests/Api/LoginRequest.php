<?php

namespace App\Http\Requests\Api;

class LoginRequest extends ApiRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return !parent::authorize();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'mail' => 'required|string|email|max:255|exists:users,mail',
            'password' => 'required|string',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'mail' => ucfirst(__('e-mail address')),
            'password' => ucfirst(__('password')),
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'mail.exists' => __('A user with the this e-mail address does not exist.'),
        ];
    }
}
