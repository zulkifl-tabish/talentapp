<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'mail' => $this->mail,
            'profile' => [
                $this->mergeWhen($this->profile, function() {
                    return new ProfileResource($this->profile);
                }),
                'cv' => [
                    $this->mergeWhen($this->cv, function() {
                        return new CvResource($this->cv);
                    }),
                    'universities' =>
                        UniversityResource::collection($this->whenLoaded('universities')),
                    'experience' => 
                        ExperienceResource::collection($this->whenLoaded('experiences')),
                    'stipendium' =>
                        StipendiumResource::collection($this->whenLoaded('stipendiums')),
                    'address' => new AddressResource($this->whenLoaded('address'))
                ]
            ],
        ];

        return $data;
    }
}
