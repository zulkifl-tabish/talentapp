<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property-read int $id
 * @property-read string $employer
 * @property-read string $work
 * @property-read \Illuminate\Support\Carbon $from
 * @property-read \Illuminate\Support\Carbon|null $to
 */
class ExperienceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employer' => $this->employer,
            'work' => $this->work,
            'from' => $this->from,
            'to' => ($to = $this->to) ? $to : null,
        ];
    }
}
