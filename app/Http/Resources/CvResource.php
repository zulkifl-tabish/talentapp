<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CvResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => $this->status,
            'phd' => (bool) $this->phd,
            'abitur' => $this->abitur,
            'birthday' => $this->birthday,
            'salaryDesired' => $this->salaryDesired
        ];
    }
}
