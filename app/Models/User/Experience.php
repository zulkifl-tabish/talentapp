<?php

namespace App\Models\User;

use App\Models\DbModel;

class Experience extends DbModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_experiences';

}
