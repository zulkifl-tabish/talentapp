<?php

namespace App\Models\User;

use App\Models\DbModel;
use Illuminate\Http\UploadedFile; 
use Illuminate\Support\Facades\Storage;

class Profile extends DbModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_profiles';


    /**
     * Uploads profile photo when pic attribute is set
     * 
     * Deletes old photo if any
     *
     * @param Illuminate\Http\UploadedFile $photo
     * @return void
     */
    public function setPicAttribute(UploadedFile $photo) {

        if(!$this->user_id) {
            return;
        }
        
        $path = $photo->store($this->user_id.'/photos', 'uploads');

        $old_phtoto = array_get($this->attributes, 'photo');

        $this->attributes['photo'] = basename($path);

        if($old_phtoto) {
            $old_phtoto_path = "{$this->user_id}/photos/{$old_phtoto}";
            $disk = Storage::disk("uploads");
            if ($disk->exists($old_phtoto_path)) {
                $disk->delete($old_phtoto_path);
            }
        }
    }

}
