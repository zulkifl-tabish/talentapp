<?php

namespace App\Models\User;

use App\Models\DbModel;

class Company extends DbModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_companies';
}
