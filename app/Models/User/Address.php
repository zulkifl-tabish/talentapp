<?php

namespace App\Models\User;

use App\Models\DbModel;

class Address extends DbModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_addresses';

}
