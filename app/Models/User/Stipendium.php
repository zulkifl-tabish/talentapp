<?php

namespace App\Models\User;

use App\Models\DbModel;

class Stipendium extends DbModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_stipendium';

    /**
     * Enable or disable create / update timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

}
