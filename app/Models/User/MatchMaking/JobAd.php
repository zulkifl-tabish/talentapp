<?php

namespace App\Models\User\MatchMaking;

use App\Models\DbModel;

class JobAd extends DbModel
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'matchmaking_job_ads';
}
