<?php

namespace App\Models\User;

use MatchMaking\Company;
use MatchMaking\JobAd;
use MatchMaking\Locality;
use App\Models\DbModel;


class MatchMaking extends DbModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_matchmakings';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\User\MatchMaking\Company
     */
    public function companies()
    {
        return $this->hasMany(Companies::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\User\MatchMaking\JobAd
     */
    public function jobAds()
    {
        return $this->hasMany(JobAd::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\User\MatchMaking\Locality
     */
    public function localities()
    {
        return $this->hasMany(Locality::class);
    }
}
