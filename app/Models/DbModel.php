<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbModel extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}
