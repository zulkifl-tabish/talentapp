<?php

namespace App;

use App\Models\User\Cv;
use App\Models\User\Profile;
use App\Models\User\Company;
use App\Models\User\Matchmaking;
use App\Models\User\Experience;
use App\Models\User\Address;
use App\Models\User\University;
use App\Models\User\Stipendium;
use App\Models\User\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mail', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Accessor for email attribute to return mail
     *
     * @return string
     */
    public function getEmailAttribute() {
        return $this->attributes['mail'];
    }
    
    /**
     * Mutator for email to set mail
     *
     * @param string $value
     * @return void
     */
    public function setEmailAttribute($value) {
        $this->attributes['mail'] = $value;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|\App\Models\User\Profile
     */
    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|\App\Models\User\Company
     */
    public function company()
    {
        return $this->hasOne(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|\App\Models\User\Cv
     */
    public function cv()
    {
        return $this->hasOne(Cv::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\User\Experience
     */
    public function experiences()
    {
        return $this->hasMany(Experience::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|\App\Models\User\Matchmaking
     */
    public function matchMaking()
    {
        return $this->hasOne(Matchmaking::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|\App\Models\User\Address
     */
    public function address()
    {
        return $this->hasOne(Address::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\User\University
     */
    public function universities()
    {
        return $this->hasMany(University::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\User\Stipendium
     */
    public function stipendiums()
    {
        return $this->hasMany(Stipendium::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\User\Role
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Loads profile relations
     *
     * @return void
     */
    public function loadProfile()
    {
        $this->load([
            'profile',
            'universities',
            'experiences',
            'company',
            'cv',
            'stipendiums',
            'matchMaking',
            'address',
        ]);

    }

    /**
     * Removes and creates new entries for specified one to many relation
     * 
     * Preferred for models having single column otherthan PK
     *
     * @param string $relation One to Many relationship name
     * @param string $column Database column of the relation
     * @param array $values values for the specified column
     * @return void
     */
    public function syncRelationByColumn(string $relation, string $column, array $values) {

       
        if(!method_exists($this, $relation)) {
            return;
        }
        
        $this->{$relation}()->delete();
        
        if($values) {
            $values = array_map(function($value) use($column) {
                return [$column => $value];
            }, $values);
            $this->{$relation}()->createMany($values);
        }
    }
}
