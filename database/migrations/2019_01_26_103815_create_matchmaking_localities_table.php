<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchmakingLocalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchmaking_localities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('matchmaking_id');
            $table->string('name');
            $table->timestamps();

            $table->foreign('matchmaking_id')
                ->references('id')
                ->on('user_matchmakings')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matchmaking_localities');
    }
}
